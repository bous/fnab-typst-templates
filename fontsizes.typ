#let tiny(t)         = text(size: 0.50em, t)
#let scriptsize(t)   = text(size: 0.70em, t)
#let footnotesize(t) = text(size: 0.80em, t)
#let small(t)        = text(size: 0.90em, t)
#let large(t)        = text(size: 1.20em, t)
#let Large(t)        = text(size: 1.44em, t)
#let LARGE(t)        = text(size: 1.73em, t)
#let huge(t)         = text(size: 2.07em, t)
#let Huge(t)         = text(size: 2.49em, t)

