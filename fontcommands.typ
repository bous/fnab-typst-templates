#let italics(..args, t) = text(style: "italic", ..args, t)
#let sf(t) = heading(level: 4, bookmarked: false, text(weight: "regular", t))
#let color(t) = highlight(t)
