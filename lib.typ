#import "fontsizes.typ": *
#import "fontcommands.typ": *

#let fnab-style(
  mainfont: "Gentium Basic",
  sffont: "Cabin",
  monofont: "Fira Code",
  fontsize: 11pt,
  accent-color: rgb("#C38514"),
  content,
) = {
  show heading: set text(
    font: sffont,
  )
  show heading.where(level: 4): {
    it => text(font: sffont, weight: "regular", it.body)
  }

  show terms.item: it => {
    text(font: sffont, weight: "bold")[#it.term:]
    h(0.6em)
    it.description
  }

  //show link: set text(font: monofont)

  set text(
    font: mainfont,
    size: fontsize,
  )

  set par(justify: true)
  set table(stroke: none)
  show raw: set text(font: monofont, size: 1.08em)
  show highlight: it => text(fill: accent-color, it.body)

  content
}

#let _maybe_pagenumber() = {
  locate(loc => {
    if counter(page).final(loc) == (1,) {""} else {
      counter(page).display()
    }
  })
}

#let article(
  author: none,
  title: none,
  style: (),
  page-style: (
    margin: (top: 3cm, x: 3.5cm),
  ),
  titlepage: none,
  body,
) = {
  show: fnab-style.with(..style)

  set page(..page-style)

  if titlepage != none {
    v(1fr)
    titlepage
    v(1fr)
    pagebreak()
  }

  set page(
    header: [#author #h(1fr) #title],
    footer: align(center, _maybe_pagenumber()),
  )

  body
}

#let letter(
  from: none,
  to: none,
  date: none,
  subject: none,
  style: (),
  body
) = {
  // Configure page and text properties.
  show: fnab-style.with(..style)
  //set page(margin: (top: 2cm))
  set page(margin: (top: 3cm, x: 3.5cm))

  let header = {
    text(9pt, if from == none { _phantom } else { from })
    v(1.8cm)
    to
    v(0.5cm)
    align(right, if date == none { _phantom } else { date })
    v(2cm)
  }

  let main = {
    // Add the subject line, if any.
    if subject != none {
      pad(right: 10%, strong(sf(subject)))
      v(1em)
    }
    body
  }

  v(-1cm)
  align(center, box(width: 16cm, align(left, header)))

  main
  //align(center, grid(columns: (35em),
  //  align(left, main)
  //))
}

#let _phantom = {hide[fg]}
